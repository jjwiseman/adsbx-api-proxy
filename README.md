<a href="https://gitlab.com/jjwiseman/adsbx-api-proxy/-/pipelines">
<img alt="pipeline status"
     src="https://gitlab.com/jjwiseman/adsbx-api-proxy/badges/master/pipeline.svg">
</a>

# adsbx-api-proxy

This is a caching proxy for the ADS-B Exchange API.

You probably don't want this. I use it because I have more than a dozen bots
hitting the ADSBX API, and I didn't want to hammer the API endpoint.

## Usage

First, this requires the nightly version of Rust (for the Rocket web framework).
See https://rocket.rs/v0.4/guide/getting-started/

```
cargo +nightly build
export ADSBX_API_KEY=<insert your key here>
./target/debug/adsbx-api-proxy https://adsbexchange.com/api/aircraft/v2/all
```

Alternately, use `release` mode:

```
cargo +nightly build --release
export ADSBX_API_KEY=<insert your key here>
./target/release/adsbx-api-proxy https://adsbexchange.com/api/aircraft/v2/all
```

By default the proxy listens on the localhost interface. To listen on another
interface, or all interfaces, you can use the `ROCKET_ADDRESS` environment
variable:

```
ROCKET_ADDRESS=0.0.0.0 ./adsbx-api-proxy https://adsbexchange.com/api/aircraft/v2/all
```

Now you can use the proxy:

```
curl https://localhost:8000/api/aircraft/v2/lat/34.1576398/lon/-118.29009629999999/dist/40
```

Note that it uses V2 of the ADSBX API. Also note that no authentication is
required—the proxy uses whatever API key is specified in the `ADSBX_API_KEY`
environment variable.

## Features

* Caches API results for up to 15 seconds.

* Can do limited translation of V2 API requests/responses to V1. That is, you
  can hit a V1 API endpoint of this proxy, and it will convert the V2 data that
  comes from ADS-B Exchange and convert into a V1-compatible form. This
  translation is sure to be incomplete, but it works for me.

* Requests `all` aircraft from the ADSBX API, and then filters those locally
  when a proximity query (e.g.
  `https://proxyhost/api/aircraft/v2/lat/34.1576398/lon/-118.29009629999999/dist/40`)
  is made. This filtering hasn't been optimized, but seems to be sufficiently
  fast, able to filter 2-3 million aircraft per second.

## Installation as a systemd service

Copy the service file:

```
sudo cp adsbx-api-proxy.service /lib/systemd/system
```

Enter the secret API key:

```
sudo systemctl edit adsbx-api-proxy
```

In the editor that opens, add the following lines:

```
[Service]
Environment=ADSBX_API_KEY=<insert your key here>
```

Secure the override file:

```
sudo chmod go-r /etc/systemd/system/adsbx-api-proxy.service.d/override.conf
```

Now enable and start the service:

```
systemctl enable adsbx-api-proxy
systemctl start adsbx-api-proxy
```


