use actix_utils::future::{ok, Either, Ready};
use actix_web::body::{EitherBody, MessageBody};
use actix_web::dev::{Service, ServiceRequest, ServiceResponse, Transform};
use actix_web::http::Method;
use actix_web::web::Data;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};

use geo::point;
use geo::{prelude::*, Point};
use log::{error, info, warn};
use reqwest::header::{HeaderMap, HeaderValue};
use serde::Deserialize;
use serde_json::json;
use std::error::Error;
use std::marker::PhantomData;
use std::panic;
use std::sync::{Arc, Mutex};
use std::task::{Context, Poll};
use std::time::{Duration, Instant};
use std::{env, fs, process};
use structopt::StructOpt;

use pin_project::pin_project;
use std::pin::Pin;

use actix_web::http::StatusCode;
use futures::{ready, Future};

#[derive(StructOpt)]
struct CliArgs {
    adsbx_api_url: String,
}

/// Holds ADS-B Exchange API credentials.
struct AdsbxCreds {
    /// An API key is required.
    api_key: String,
    /// A whitelist token is optional.
    whitelist_token: Option<String>,
}

impl AdsbxCreds {
    /// Creates AdsbxCreds from environment variables.
    fn from_env() -> AdsbxCreds {
        AdsbxCreds {
            api_key: env::var("ADSBX_API_KEY").expect("ADSBX_API_KEY env var not set"),
            whitelist_token: env::var("ADSBX_WHITELIST").ok(),
        }
    }
}

// I used https://stackoverflow.com/a/71790578/122762 as the template for this.
// My god, thank god for stackoverflow.
pub struct Authentication;

impl<S, B> Transform<S, ServiceRequest> for Authentication
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = actix_web::Error>,
    S::Future: 'static,
    B: MessageBody,
{
    type Response = ServiceResponse<EitherBody<B>>;
    type Error = actix_web::Error;
    type InitError = ();
    type Transform = AuthenticationMiddleware<S>;
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        env::var("PROXY_API_KEY").expect("PROXY_API_KEY env var not set");
        ok(AuthenticationMiddleware { service })
    }
}
pub struct AuthenticationMiddleware<S> {
    service: S,
}

impl<S, B> Service<ServiceRequest> for AuthenticationMiddleware<S>
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = actix_web::Error>,
    S::Future: 'static,
    B: MessageBody,
{
    type Response = ServiceResponse<EitherBody<B>>;

    type Error = actix_web::Error;

    type Future = Either<AuthenticationFuture<S, B>, Ready<Result<Self::Response, Self::Error>>>;

    fn poll_ready(&self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.service.poll_ready(cx)
    }

    fn call(&self, req: ServiceRequest) -> Self::Future {
        let mut authenticate_pass = false;

        if Method::OPTIONS == *req.method() {
            authenticate_pass = true;
        }

        let auth = req.headers().get("proxy-api-key");
        let api_key = env::var("PROXY_API_KEY").expect("PROXY_API_KEY env var not set");
        if auth.is_some() && *auth.unwrap() == api_key {
            authenticate_pass = true;
        }

        if authenticate_pass {
            Either::left(AuthenticationFuture {
                fut: self.service.call(req),
                _phantom: PhantomData,
            })
        } else {
            let res = HttpResponse::with_body(StatusCode::UNAUTHORIZED, "Invalid API key");
            Either::right(ok(req
                .into_response(res)
                .map_into_boxed_body()
                .map_into_right_body()))
        }
    }
}

#[pin_project]
pub struct AuthenticationFuture<S, B>
where
    S: Service<ServiceRequest>,
{
    #[pin]
    fut: S::Future,
    _phantom: PhantomData<B>,
}

impl<S, B> Future for AuthenticationFuture<S, B>
where
    B: MessageBody,
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = actix_web::Error>,
{
    type Output = Result<ServiceResponse<EitherBody<B>>, actix_web::Error>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let res = match ready!(self.project().fut.poll(cx)) {
            Ok(res) => res,
            Err(err) => return Poll::Ready(Err(err.into())),
        };

        Poll::Ready(Ok(res.map_into_left_body()))
    }
}

struct AppContext {
    latest_converted_aircraft_data: Option<serde_json::Value>,
    latest_aircraft_data: Option<serde_json::Value>,
    adsbx_fetch_time: Option<Instant>,
    max_cache_age: Duration,
    adsbx_api_url: String,
    adsbx_creds: AdsbxCreds,
}

#[derive(Clone)]
struct SharedState {
    context: Arc<Mutex<AppContext>>,
}

// --------------------
// V2 API endpoints
// --------------------

// Returns all aircraft.
//
// Example:
// https://proxyhost/api/aircraft/v2/all

async fn web_v2_get_all(state: web::Data<SharedState>) -> impl Responder {
    info!("Getting all aircraft");
    let shared = state.get_ref();
    let result = {
        let mut context = shared.context.lock().unwrap();
        get_adsbx_data(&mut context).await;
        match &context.latest_aircraft_data {
            Some(v) => v.to_string(),
            None => String::from("{ac: []}"),
        }
    };
    HttpResponse::Ok()
        .content_type("application/json")
        .body(result)
}

// Returns all aircraft within a geographic circle. The URL must contain the
// coordinates of the circle's center and its radius.
//
// Example:
// https://proxyhost/api/aircraft/v2/lat/34.1576398/lon/-118.29009629999999/dist/40
//
// Distance is in nautical miles.

#[derive(Deserialize)]
struct NearbyParams {
    lat: f64,
    lon: f64,
    dist: f64,
}

async fn web_v2_get_nearby(
    params: web::Path<NearbyParams>,
    state: web::Data<SharedState>,
) -> impl Responder {
    let params = params.into_inner();
    let shared = state.get_ref();
    let result = {
        let mut context = shared.context.lock().unwrap();
        get_adsbx_data(&mut context).await;
        match &context.latest_aircraft_data {
            Some(v) => {
                let center = point!(x: params.lon, y: params.lat);
                let neighbors = ac_nearby(center, params.dist, v["ac"].as_array().unwrap());
                json!({
                    "now": v["now"],
                    "ctime": v["ctime"],
                    "msg": "No error",
                    "ptime": v["ptime"],
                    "total": neighbors.len(),
                    "ac": neighbors
                })
                .to_string()
            }
            None => json!({ "ac": [] }).to_string(),
        }
    };
    HttpResponse::Ok()
        .content_type("application/json")
        .body(result)
}

// --------------------
// V1 API endpoints
// --------------------

// Same as web_v2_get_nearby but results are returned in V1 format.

async fn web_v1_get_nearby(
    params: web::Path<NearbyParams>,
    state: web::Data<SharedState>,
) -> impl Responder {
    let params = params.into_inner();
    let shared = state.get_ref();
    let result = {
        let mut context = shared.context.lock().unwrap();
        get_adsbx_data(&mut context).await;
        match &context.latest_converted_aircraft_data {
            Some(v) => {
                let center = point!(x: params.lon, y: params.lat);
                let neighbors = ac_nearby(center, params.dist, v["ac"].as_array().unwrap());
                json!({ "ac": neighbors }).to_string()
            }
            _ => json!({ "ac": [] }).to_string(),
        }
    };
    HttpResponse::Ok()
        .content_type("application/json")
        .body(result)
}

// V1 of the ADSBX API doesn't have an "all" endpoint (that I know of),
// but we offer one here.

async fn web_v1_get_all(state: web::Data<SharedState>) -> impl Responder {
    let shared = state.get_ref();
    let result = {
        let mut context = shared.context.lock().unwrap();
        get_adsbx_data(&mut context).await;
        match &context.latest_converted_aircraft_data {
            Some(v) => {
                let matches = ac_matching_icaos(vec![], v["ac"].as_array().unwrap());
                json!({ "ac": matches }).to_string()
            }
            _ => String::from("{\"ac\": []}"),
        }
    };
    HttpResponse::Ok()
        .content_type("application/json")
        .body(result)
}

const METERS_PER_NM: f64 = 1852.0;

// Given a point, a distance, and a vector of (JSON objects representing)
// aircraft, return a new vector of (JSON objects representing aircraft) that
// are within that distance of the point.
//
// Totally unoptimized, but seems plenty fast enough.

fn ac_nearby(
    point: Point<f64>,
    dist_nm: f64,
    all_ac: &[serde_json::Value],
) -> Vec<&serde_json::Value> {
    let start = Instant::now();
    let max_dist_meters = dist_nm * METERS_PER_NM;
    let neighbors: Vec<&serde_json::Value> = all_ac
        .iter()
        .filter(|ac| {
            let ac_lat: Option<f64> = match &ac["lat"] {
                v if v.is_f64() => v.as_f64(),
                v if v.is_string() => v.as_str().unwrap().parse().ok(),
                _ => None,
            };
            let ac_lon: Option<f64> = match &ac["lon"] {
                v if v.is_f64() => v.as_f64(),
                v if v.is_string() => v.as_str().unwrap().parse().ok(),
                _ => None,
            };
            if ac_lat.is_some() && ac_lon.is_some() {
                let p = point!(x: ac_lon.unwrap(), y: ac_lat.unwrap());
                let distance = point.haversine_distance(&p);
                distance < max_dist_meters
            } else {
                false
            }
        })
        .collect();
    let end = Instant::now();
    info!(
        "Found {} aircraft within {} nm of {:?} out of {} in {:?} ({} aircraft/s)",
        neighbors.len(),
        dist_nm,
        point,
        all_ac.len(),
        end - start,
        1_000_000.0 * (all_ac.len() as f64 / ((end - start).as_micros() as f64))
    );
    neighbors
}

fn ac_matching_icaos(
    allowed_icaos: Vec<String>,
    all_ac: &[serde_json::Value],
) -> Vec<&serde_json::Value> {
    let start = Instant::now();
    let matches: Vec<&serde_json::Value> = all_ac
        .iter()
        .filter(|ac| {
            let ac_icao: Option<String> = match &ac["icao"] {
                v if v.is_string() => v.as_str().unwrap().parse().ok(),
                _ => None,
            };
            if let Some(icao) = ac_icao {
                allowed_icaos.contains(&icao)
            } else {
                false
            }
        })
        .collect();
    let end = Instant::now();
    info!(
        "Found {} aircraft matching {:?} out of {} in {:?} ({} aircraft/s)",
        matches.len(),
        allowed_icaos,
        all_ac.len(),
        end - start,
        1_000_000.0 * (all_ac.len() as f64 / ((end - start).as_micros() as f64))
    );

    matches
}

async fn get_adsbx_data(context: &mut AppContext) {
    let now = Instant::now();
    let cache_time = context.adsbx_fetch_time;
    if cache_time.is_none()
        || (now.saturating_duration_since(cache_time.unwrap()) > context.max_cache_age)
    {
        let adsbx_data = match true {
            true => fetch_adsbx_data(&context.adsbx_api_url, &context.adsbx_creds).await,
            false => read_adsbx_data(),
        };
        match adsbx_data {
            Ok(v) => {
                if let Some(msg) = v["msg"].as_str() {
                    warn!("Message from adsbx: {}", msg);
                }
                let converted_data = convert_adsbx_v2_to_v1(&v);
                context.latest_aircraft_data = Some(v);
                context.latest_converted_aircraft_data = Some(converted_data);
            }
            Err(err) => {
                context.latest_aircraft_data = None;
                context.latest_converted_aircraft_data = None;
                error!(
                    "Error fetching data from ADSBX API at {}: {}",
                    context.adsbx_api_url, err
                );
            }
        }
        context.adsbx_fetch_time = Some(now);
    }
}

fn headers_from_creds(creds: &AdsbxCreds) -> HeaderMap {
    let mut headers = HeaderMap::new();
    headers.insert(
        "api-auth",
        HeaderValue::from_str(&creds.api_key).expect("Invalid characters in API key"),
    );
    if creds.whitelist_token.is_some() {
        headers.insert(
            "ADSBX-WL",
            HeaderValue::from_str(creds.whitelist_token.as_ref().unwrap())
                .expect("Invalid characters in API whitelist token"),
        );
    }
    headers
}

// Fetches data from the ADSBX V2 API.

async fn fetch_adsbx_data(
    url: &str,
    creds: &AdsbxCreds,
) -> Result<serde_json::Value, Box<dyn Error>> {
    info!("Getting ADSBX data");
    let client = reqwest::Client::builder()
        .user_agent("adsbx-api-proxy / 0.0")
        .gzip(true)
        .default_headers(headers_from_creds(creds))
        .build()?;
    let req_start = Instant::now();
    let resp = client.get(url).send().await?;
    let resp = match resp.error_for_status() {
        Ok(r) => r,
        Err(err) => return Err(err.into()),
    };
    let body = resp.text().await?;
    let req_end = Instant::now();
    let start = Instant::now();
    let json_value: serde_json::Value = match serde_json::from_str(&body) {
        Ok(v) => v,
        Err(err) => {
            error!(
                "Error parsing JSON: {:?} ({} chars) {}",
                err,
                body.len(),
                &body
            );
            return Err(err.into());
        }
    };
    let end = Instant::now();
    info!(
        "Fetched JSON in {:?}, parsed JSON in {:?}",
        req_end - req_start,
        end - start
    );
    Ok(json_value)
}

// Converts the (JSON representation of an aircraft) from the V2 representation
// to the V1 representation. Not complete--just does enough to make the Advisory
// Circular bots happy.

fn convert_target(target: &serde_json::Value, postime: u64) -> serde_json::Value {
    let alt_baro = &target["alt_baro"];
    let mut alt_baro_cvt = String::from("");
    let mut gnd_cvt = String::from("0");
    if *alt_baro == json!("ground") {
        gnd_cvt = String::from("1");
        alt_baro_cvt = String::from("0");
    } else if *alt_baro != json!(null) {
        alt_baro_cvt = alt_baro.to_string();
    }
    let alt_geom_cvt = match &target["alt_geom"] {
        v if *v == json!(null) => String::from(""),
        v => v.to_string(),
    };
    let gs_cvt = match &target["gs"] {
        v if *v == json!(null) => String::from(""),
        v => v.to_string(),
    };
    let squawk_cvt = match &target["squawk"] {
        v if *v == json!(null) => json!(""),
        v => v.clone(),
    };
    let flight_cvt = match &target["flight"] {
        v if *v == json!(null) => json!(""),
        v => match v.as_str() {
            Some(f) => json!(f.to_string().trim()),
            None => {
                error!("flight should be a string: {}", v);
                json!("")
            }
        },
    };
    let converted = json!({
        "lat": target["lat"].to_string(),
        "lon": target["lon"].to_string(),
        "icao": target["hex"].as_str().unwrap().to_string().to_uppercase(),
        "reg": "",
        "alt": alt_baro_cvt,
        "galt": alt_geom_cvt,
        "gnd": gnd_cvt,
        "spd": gs_cvt,
        "sqk": squawk_cvt,
        "call": flight_cvt,
        "postime": postime.to_string()
    });
    converted
}

// Converts a full V2 API payload to V1.

fn convert_adsbx_v2_to_v1(result: &serde_json::Value) -> serde_json::Value {
    let ctime = &result["ctime"];
    let postime = match ctime {
        v if v.is_i64() => v.as_i64().unwrap() as u64,
        v if v.is_string() => v.as_str().unwrap().parse().unwrap(),
        _ => panic!("ctime is an unexpected type: {ctime}", ctime = ctime),
    };
    let converted_targets: Vec<serde_json::Value> = result["ac"]
        .as_array()
        .unwrap()
        .iter()
        .map(|x| convert_target(x, postime))
        .collect();
    json!({ "ac": converted_targets })
}

// Reads JSON from a file.

fn read_adsbx_data() -> Result<serde_json::Value, Box<dyn Error>> {
    let path = "adsbx-api-v2-all.json";
    info!("Reading {}", path);
    let s: String = fs::read_to_string(path)?;
    let json: serde_json::Value = serde_json::from_str(&s)?;
    Ok(json)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    let args = CliArgs::from_args();
    // If any thread panics, exit the process.
    let orig_hook = panic::take_hook();
    std::panic::set_hook(Box::new(move |panic_info| {
        orig_hook(panic_info);
        error!("Aborting");
        process::exit(1);
    }));
    let context = Arc::new(Mutex::new(AppContext {
        latest_aircraft_data: None,
        latest_converted_aircraft_data: None,
        adsbx_fetch_time: None,
        max_cache_age: Duration::from_secs(15),
        adsbx_api_url: args.adsbx_api_url,
        adsbx_creds: AdsbxCreds::from_env(),
    }));

    let shared_state = SharedState {
        context: context.clone(),
    };

    info!("Starting server on port 8080.");
    HttpServer::new(move || {
        App::new()
            .app_data(Data::new(shared_state.clone()))
            .wrap(Authentication)
            .route("/api/aircraft/v2/all", web::get().to(web_v2_get_all))
            .route(
                "/api/aircraft/v2/lat/{lat}/lon/{lon}/dist/{dist}",
                web::get().to(web_v2_get_nearby),
            )
            .route(
                "/api/aircraft/json/lat/{lat}/lon/{lon}/dist/{dist}",
                web::get().to(web_v1_get_nearby),
            )
            .route("/api/aircraft/json/all", web::get().to(web_v1_get_all))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
